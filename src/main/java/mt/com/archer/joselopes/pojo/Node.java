package mt.com.archer.joselopes.pojo;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Node {

    private int value;
    private List<Integer> path = new LinkedList<>();

    public Node(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("Cannot load a negative path.");
        }
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public List<Integer> getPath() {
        return path;
    }

    /**
     * Has no path.
     */
    public void setRoot() {
        path.add(value);
    }

    /**
     * Associate with parent's path.
     */
    public void setCompletePath(Node parentNode) {
        if (path.isEmpty()) {
            parentNode.getPath().stream().forEach(path::add);
            path.add(value);
        } else {
            int newPath = parentNode.getPathWeight();
            int oldPath = getPathWeight();
            if (newPath + value < oldPath) {
                path = new LinkedList<>();
                parentNode.getPath().stream().forEach(path::add);
                path.add(value);
            }
        }
    }

    /**
     * Returns the sum of node values from root to current.
     *
     * @return total distance
     */
    public int getPathWeight() {
        return path.parallelStream().reduce(0, Integer::sum);
    }

    public String showPath() {
        return path.stream().map(Object::toString).collect(Collectors.joining(" + "));
    }

}
