package mt.com.archer.joselopes;

import mt.com.archer.joselopes.executor.Datasource;
import mt.com.archer.joselopes.pojo.Node;

import java.io.IOException;
import java.util.Optional;
import java.util.Scanner;

public class MinTrianglePath {

    public static void main(String[] args) {
        Datasource datasource;

        if (args != null && args.length > 0) {
            datasource = getDatasourceFromFile(args);
        } else {
            datasource = getDatasourceFromUserInput();
        }

        final Optional<Node> nearestNode = datasource.getNearestNode();
        if (nearestNode.isPresent()) {
            System.out.println("Minimal path is: " + nearestNode.get().showPath() + " = " + nearestNode.get().getPathWeight());
        } else {
            System.out.println("No minimal path was found!");
        }
    }

    private static Datasource getDatasourceFromUserInput() {
        final Scanner userInput = new Scanner(System.in);
        return new Datasource(userInput);
    }

    private static Datasource getDatasourceFromFile(String[] args) throws RuntimeException {
        try {
            return new Datasource(args[0]);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
