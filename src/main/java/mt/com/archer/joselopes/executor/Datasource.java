package mt.com.archer.joselopes.executor;

import mt.com.archer.joselopes.pojo.Node;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Datasource {

    private static final String EOF = "EOF";
    private static final Pattern ACCEPTED_PATTERN = Pattern.compile("^((\\s)*(\\d)+(\\s)*)+$|^EOF$");

    private boolean loadOpen = true;
    private List<Node> lastLineLoaded = new LinkedList<>();

    /**
     * Just for tests purposes.
     */
    protected Datasource() {
    }

    public Datasource(final String file) throws IOException {
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            stream.map(String::trim).forEach(this::loadLine);
        }
    }

    public Datasource(final Scanner scanner) {
        String line;
        do {
            line = scanner.nextLine();
            loadLine(line.trim());
        } while (!EOF.equals(line));
        scanner.close();
    }

    public void loadLine(final String line) {
        if (!ACCEPTED_PATTERN.matcher(line).matches()) {
            throw new IllegalArgumentException("Invalid input: please inform only numbers (separated by spaces) or 'EOF' to indicate the end of file.");
        }

        if (EOF.equals(line)) {
            loadOpen = false;
        }

        if (!isLoadOpen()) {
            return;
        }

        // load all integers from line
        final IntStream integers = Stream.of(line.split("\\s")).mapToInt(Integer::parseInt);

        // create a list of nodes based on input line
        final List<Node> currentNodes = new LinkedList<>();
        integers.mapToObj(Node::new).forEach(currentNodes::add);

        checkInputData(currentNodes);

        // if there is no nodes loaded yet, set current as root
        if (lastLineLoaded.isEmpty()) {
            currentNodes.stream().forEach(Node::setRoot);
        }

        // for each loaded node (if no one exists yet this 'range' will be skipped): set path to children
        IntStream.range(0, lastLineLoaded.size()).forEach(i -> {
            Node parentNode = lastLineLoaded.get(i);
            currentNodes.get(i).setCompletePath(parentNode);
            currentNodes.get(i + 1).setCompletePath(parentNode);
        });

        // set the current list of nodes as the last executed
        lastLineLoaded = currentNodes;
    }

    private void checkInputData(final List<Node> currentNodes) {
        if (lastLineLoaded.isEmpty() && currentNodes.size() > 1) {
            throw new IllegalArgumentException("Data must start with only one number: the starting node.");
        }
        if (lastLineLoaded.size() + 1 != currentNodes.size()) {
            final String error = String.format("Line [%d] must have one more element than line [%d].", lastLineLoaded.size() + 1, lastLineLoaded.size());
            throw new IllegalArgumentException(error);
        }
    }

    public boolean isLoadOpen() {
        return loadOpen;
    }

    public List<Node> getLastLineLoaded() {
        return lastLineLoaded.stream().collect(Collectors.toList());
    }

    /**
     * It's possible to select the current nearest node any time: only the current last line will be taken into account.
     *
     * @return the nearest node (if any)
     */
    public Optional<Node> getNearestNode() {
        return lastLineLoaded.stream().sorted(Comparator.comparing(node -> node.getPathWeight())).findFirst();
    }

}
