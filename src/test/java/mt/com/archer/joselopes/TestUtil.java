package mt.com.archer.joselopes;

import java.io.File;

public class TestUtil {

    public TestUtil() {}

    public String getFileFromClasspath(String fileName) {
        final ClassLoader classLoader = getClass().getClassLoader();
        final File file = new File(classLoader.getResource(fileName).getFile());
        return file.getAbsolutePath();
    }

}
