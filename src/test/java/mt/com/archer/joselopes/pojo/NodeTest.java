package mt.com.archer.joselopes.pojo;

import org.junit.Assert;
import org.junit.Test;

public class NodeTest {

    @Test
    public void testNewNode() {
        // given
        int value = 10;

        // when
        Node node = new Node(value);

        // than
        Assert.assertEquals(value, node.getValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNewNodeCannotBeNegative() {
        // given
        int value = -10;

        // when
        new Node(value);
    }

    @Test
    public void testRootNode() {
        // given
        int value1 = 1;
        Node node1 = new Node(value1);

        Assert.assertEquals(0, node1.getPath().size());

        // when
        node1.setRoot();

        // than
        Assert.assertEquals(1, node1.getPath().size());
    }

    @Test
    public void testCompletePath() {
        // given
        int value1 = 1;
        int value2 = 2;
        int value3 = 3;
        Node node1 = new Node(value1);
        Node node2 = new Node(value2);
        Node node3 = new Node(value3);

        // when
        node1.setRoot();

        node2.setCompletePath(node1);
        node3.setCompletePath(node1);

        // than
        Assert.assertEquals(1, node1.getPath().size());
        Assert.assertEquals(2, node2.getPath().size());
        Assert.assertEquals(2, node3.getPath().size());
    }

    @Test
    public void testPathSum() {
        // given
        int value1 = 1;
        int value2 = 2;
        int value3 = 3;
        Node node1 = new Node(value1);
        Node node2 = new Node(value2);
        Node node3 = new Node(value3);

        // when
        node1.setRoot();

        node2.setCompletePath(node1);
        node3.setCompletePath(node1);

        // than
        Assert.assertEquals(1, node1.getPathWeight());
        Assert.assertEquals(3, node2.getPathWeight());
        Assert.assertEquals(4, node3.getPathWeight());
    }

    @Test
    public void testShowPath() {
        // given
        int value1 = 1;
        int value2 = 2;
        int value3 = 3;
        Node node1 = new Node(value1);
        Node node2 = new Node(value2);
        Node node3 = new Node(value3);

        // when
        node1.setRoot();

        node2.setCompletePath(node1);
        node3.setCompletePath(node1);

        // than
        Assert.assertEquals("1", node1.showPath());
        Assert.assertEquals("1 + 2", node2.showPath());
        Assert.assertEquals("1 + 3", node3.showPath());
    }

}
