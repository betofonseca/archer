package mt.com.archer.joselopes.executor;

import mt.com.archer.joselopes.TestUtil;
import mt.com.archer.joselopes.pojo.Node;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class DatasourceTest {

    private TestUtil testUtil = new TestUtil();

    @Test
    public void testLoadOneLine() {
        // given
        final Datasource datasource = new Datasource();
        final String line = "1";

        // when
        datasource.loadLine(line);

        // than
        final List<Node> data = datasource.getLastLineLoaded();

        // number of lines
        Assert.assertEquals(1, data.size());
        // number of collumns
        Assert.assertEquals(1, data.size());
        // data inside column
        Node node1 = data.get(0);
        Assert.assertEquals(1, node1.getValue());
        Assert.assertEquals(1, node1.getPath().size());

        Assert.assertTrue(datasource.isLoadOpen());
    }

    @Test
    public void testLoadTwoLines() {
        // given
        final Datasource datasource = new Datasource();
        final String line1 = "1";
        final String line2 = "1 2";

        // when
        datasource.loadLine(line1);
        datasource.loadLine(line2);

        // than
        final List<Node> data = datasource.getLastLineLoaded();

        // number of collumns
        Assert.assertEquals(2, data.size());
        // data inside column
        Node node1 = data.get(0);
        Assert.assertEquals(1, node1.getValue());
        Assert.assertEquals(2, node1.getPath().size());
        // line2: dont have adjacent nodes because it's the last line
        Node node2 = data.get(1);
        Assert.assertEquals(2, node2.getValue());
        Assert.assertEquals(2, node2.getPath().size());
        // check if datasource can receive more data
        Assert.assertTrue(datasource.isLoadOpen());
    }

    @Test
    public void testLoadMultiLines() {
        // given
        final Datasource datasource = new Datasource();
        final String line1 = "1";
        final String line2 = "1 2";
        final String line3 = "1 2 3";
        final String line4 = "1 2 3 4";
        final String line5 = "1 2 3 4 5";

        // when
        datasource.loadLine(line1);
        datasource.loadLine(line2);
        datasource.loadLine(line3);
        datasource.loadLine(line4);
        datasource.loadLine(line5);

        // than
        final List<Node> data = datasource.getLastLineLoaded();

        // number of collumns
        Assert.assertEquals(5, data.size());
        // data inside column
        Node node1 = data.get(0);
        Assert.assertEquals(1, node1.getValue());
        Assert.assertEquals(5, node1.getPath().size());
        Node node2 = data.get(1);
        Assert.assertEquals(2, node2.getValue());
        Assert.assertEquals(5, node2.getPath().size());
        Node node3 = data.get(2);
        Assert.assertEquals(3, node3.getValue());
        Assert.assertEquals(5, node3.getPath().size());
        Node node4 = data.get(3);
        Assert.assertEquals(4, node4.getValue());
        Assert.assertEquals(5, node4.getPath().size());
        Node node5 = data.get(4);
        Assert.assertEquals(5, node5.getValue());
        Assert.assertEquals(5, node5.getPath().size());

        // check if datasource can receive more data
        Assert.assertTrue(datasource.isLoadOpen());
    }

    @Test
    public void testLoadNumbersOrEOF() {
        // given
        final Datasource datasource = new Datasource();
        final String line1 = "1";
        final String line2 = "1 2";
        final String line3 = "EOF";

        // when
        datasource.loadLine(line1);
        datasource.loadLine(line2);
        datasource.loadLine(line3);

        // than
        final List<Node> data = datasource.getLastLineLoaded();

        // number of collumns
        Assert.assertEquals(2, data.size());
        // data inside column
        Assert.assertEquals(1, data.get(0).getValue());
        Assert.assertEquals(2, data.get(1).getValue());
        // check if datasource can receive more data
        Assert.assertFalse(datasource.isLoadOpen());
    }

    @Test
    public void testLoadOnlyNumbersOrEOF() {
        // given
        final Datasource datasource = new Datasource();

        // when
        datasource.loadLine("1");

        // special caracter is not accepted
        try {
            datasource.loadLine("!");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Invalid input: please inform only numbers (separated by spaces) or 'EOF' to indicate the end of file.", e.getMessage());
        }

        // letter is not accepted
        try {
            datasource.loadLine("A");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Invalid input: please inform only numbers (separated by spaces) or 'EOF' to indicate the end of file.", e.getMessage());
        }

        // text is not accepted
        try {
            datasource.loadLine("AAAAAAA");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Invalid input: please inform only numbers (separated by spaces) or 'EOF' to indicate the end of file.", e.getMessage());
        }

        // can only provide numbers OR EOF: must be on different lines
        try {
            datasource.loadLine("1EOF");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Invalid input: please inform only numbers (separated by spaces) or 'EOF' to indicate the end of file.", e.getMessage());
        }

        // can only provide numbers OR EOF: must be on different lines
        try {
            datasource.loadLine("1 EOF");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Invalid input: please inform only numbers (separated by spaces) or 'EOF' to indicate the end of file.", e.getMessage());
        }

        // EOF is case sensitive
        try {
            datasource.loadLine("eof");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Invalid input: please inform only numbers (separated by spaces) or 'EOF' to indicate the end of file.", e.getMessage());
        }

        // than
        final List<Node> data = datasource.getLastLineLoaded();

        // number of lines
        Assert.assertEquals(1, data.size());
        // number of collumns
        Assert.assertEquals(1, data.size());
        // data inside column
        Assert.assertEquals(1, data.get(0).getValue());
        // check if datasource can receive more data
        Assert.assertTrue(datasource.isLoadOpen());
    }

    @Test
    public void testDontLoadMoreDataAfterEOF() {
        // given
        final Datasource datasource = new Datasource();
        final String line1 = "1";
        final String line2 = "1 2";
        final String line3 = "EOF";
        final String line4 = "1 2";

        // when
        datasource.loadLine(line1);
        datasource.loadLine(line2);
        datasource.loadLine(line3);
        datasource.loadLine(line4);

        // than
        final List<Node> data = datasource.getLastLineLoaded();

        // number of collumns
        Assert.assertEquals(2, data.size());
        // data inside column
        Assert.assertEquals(1, data.get(0).getValue());
        Assert.assertEquals(2, data.get(1).getValue());
        // check if datasource can receive more data
        Assert.assertFalse(datasource.isLoadOpen());
    }

    /**
     * Each line MUST HAVE one more number than the last one.
     */
    @Test
    public void testLoadIncrementalLines() {
        // given
        final Datasource datasource = new Datasource();
        final String line1 = "1";
        final String line2 = "1 2";
        final String line3 = "1 2";

        try {
            // when
            datasource.loadLine(line1);
            datasource.loadLine(line2);
            datasource.loadLine(line3);

            Assert.fail("The datasource didn't trigger the exception.");
        } catch (IllegalArgumentException e) {
            // than
            Assert.assertEquals("Line [3] must have one more element than line [2].", e.getMessage());
        }
    }

    /**
     * Each line MUST HAVE one more number than the last one.
     */
    @Test
    public void testFirstLineMustHaveOneNode() {
        // given
        final Datasource datasource = new Datasource();
        final String line1 = "1 2";
        final String line2 = "1 2 3";

        try {
            // when
            datasource.loadLine(line1);
            datasource.loadLine(line2);

            Assert.fail("The datasource didn't trigger the exception.");
        } catch (IllegalArgumentException e) {
            // than
            Assert.assertEquals("Data must start with only one number: the starting node.", e.getMessage());
        }
    }

    @Test
    public void testCreateDatasourceFromCorrectFile() throws IOException {
        // given
        final String fileCompleteName = testUtil.getFileFromClasspath("correctFile.txt");

        // when
        Datasource datasource = new Datasource(fileCompleteName);
        final List<Node> data = datasource.getLastLineLoaded();

        // than
        // number of collumns
        Assert.assertEquals(3, data.size());
        // data inside column
        Node node1 = data.get(0);
        Assert.assertEquals(1, node1.getValue());
        Assert.assertEquals(3, node1.getPath().size());
        Node node2 = data.get(1);
        Assert.assertEquals(2, node2.getValue());
        Assert.assertEquals(3, node2.getPath().size());
        Node node3 = data.get(2);
        Assert.assertEquals(3, node3.getValue());
        Assert.assertEquals(3, node3.getPath().size());
    }

    @Test
    public void testCreateDatasourceFromIncorrectFile() {
        try {
            final String fileCompleteName = testUtil.getFileFromClasspath("incorrectFile.txt");
            new Datasource(fileCompleteName);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Invalid input: please inform only numbers (separated by spaces) or 'EOF' to indicate the end of file.", e.getMessage());
        } catch (Exception e) {
            Assert.fail("The file was not found to do the test.");
        }
    }

    @Test(expected = IOException.class)
    public void testCreateDatasourceFromNotExistingFile() throws IOException {
        Datasource datasource = new Datasource("/this/path/dont/exists.txt");
        Assert.assertEquals(3, datasource.getLastLineLoaded().size());
    }

    @Test
    public void testCreateDatasourceFromUserInput() {
        //given
        String userInput = "1\n1 2\n1 2 3\nEOF\n";
        InputStream stdin = System.in;
        try {
            final InputStream input = new ByteArrayInputStream(userInput.getBytes());
            System.setIn(input);
            Scanner scanner = new Scanner(System.in);

            // when
            Datasource datasource = new Datasource(scanner);
            final List<Node> data = datasource.getLastLineLoaded();

            // than
            // number of collumns
            Assert.assertEquals(3, data.size());
            // data inside column
            Node node1 = data.get(0);
            Assert.assertEquals(1, node1.getValue());
            Assert.assertEquals(3, node1.getPath().size());
            Node node2 = data.get(1);
            Assert.assertEquals(2, node2.getValue());
            Assert.assertEquals(3, node2.getPath().size());
            Node node3 = data.get(2);
            Assert.assertEquals(3, node3.getValue());
            Assert.assertEquals(3, node3.getPath().size());

            // check if datasource can receive more data
            Assert.assertFalse(datasource.isLoadOpen());
        } finally {
            System.setIn(stdin);
        }
    }

    @Test
    public void checkFileWith10Lines() throws IOException {
        // given
        final String fileCompleteName = testUtil.getFileFromClasspath("data10.txt");

        // when
        Datasource datasource = new Datasource(fileCompleteName);
        final List<Node> data = datasource.getLastLineLoaded();

        // than
        // number of collumns
        Assert.assertEquals(10, data.size());
        // data inside column
        Node node1 = data.get(0);
        Assert.assertEquals(1, node1.getValue());
        Assert.assertEquals(10, node1.getPath().size());
        Node node2 = data.get(1);
        Assert.assertEquals(2, node2.getValue());
        Assert.assertEquals(10, node2.getPath().size());
        Node node3 = data.get(2);
        Assert.assertEquals(3, node3.getValue());
        Assert.assertEquals(10, node3.getPath().size());
        Node node4 = data.get(3);
        Assert.assertEquals(4, node4.getValue());
        Assert.assertEquals(10, node4.getPath().size());
        Node node5 = data.get(4);
        Assert.assertEquals(5, node5.getValue());
        Assert.assertEquals(10, node5.getPath().size());
        Node node6 = data.get(5);
        Assert.assertEquals(6, node6.getValue());
        Assert.assertEquals(10, node6.getPath().size());
        Node node7 = data.get(6);
        Assert.assertEquals(7, node7.getValue());
        Assert.assertEquals(10, node7.getPath().size());
        Node node8 = data.get(7);
        Assert.assertEquals(8, node8.getValue());
        Assert.assertEquals(10, node8.getPath().size());
        Node node9 = data.get(8);
        Assert.assertEquals(9, node9.getValue());
        Assert.assertEquals(10, node9.getPath().size());
        Node node10 = data.get(9);
        Assert.assertEquals(10, node10.getValue());
        Assert.assertEquals(10, node10.getPath().size());

        final Optional<Node> nearestNode = datasource.getNearestNode();
        Assert.assertTrue(nearestNode.isPresent());
        Assert.assertEquals(10, nearestNode.get().getPathWeight());
    }

    @Test
    public void checkFileWith50Lines() throws IOException {
        // given
        final String fileCompleteName = testUtil.getFileFromClasspath("data50.txt");

        // when
        Datasource datasource = new Datasource(fileCompleteName);
        final List<Node> data = datasource.getLastLineLoaded();

        // than
        // number of collumns
        Assert.assertEquals(50, data.size());

        final Optional<Node> nearestNode = datasource.getNearestNode();
        Assert.assertTrue(nearestNode.isPresent());
        Assert.assertEquals(50, nearestNode.get().getPathWeight());
    }

    @Test
    public void checkFileWith500Lines() throws IOException {
        // given
        final String fileCompleteName = testUtil.getFileFromClasspath("data500.txt");

        // when
        Datasource datasource = new Datasource(fileCompleteName);
        final List<Node> data = datasource.getLastLineLoaded();

        // than
        // number of collumns
        Assert.assertEquals(500, data.size());

        final Optional<Node> nearestNode = datasource.getNearestNode();
        Assert.assertTrue(nearestNode.isPresent());
        Assert.assertEquals(500, nearestNode.get().getPathWeight());
    }

}
