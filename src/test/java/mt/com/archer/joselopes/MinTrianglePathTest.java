package mt.com.archer.joselopes;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class MinTrianglePathTest {

    private TestUtil testUtil = new TestUtil();

    @Test
    public void testMainWithFile() {
        final String fileFromClasspath = testUtil.getFileFromClasspath("data10.txt");
        final String[] args = new String[]{fileFromClasspath};
        MinTrianglePath.main(args);
    }

    @Test(expected = RuntimeException.class)
    public void testMainWithIncorrectFile() {
        final String[] args = new String[]{"/path/to/non/existing/file.txt"};
        MinTrianglePath.main(args);
    }

    @Test
    public void testMainWithUserInput() {
        // given
        String userInput = "1\n1 2\n1 2 3\nEOF\n";
        final InputStream input = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(input);

        final InputStream original = System.in;
        System.setIn(input);

        // when
        MinTrianglePath.main(null);

        // than
        System.setIn(original);
    }

}
