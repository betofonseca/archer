# MinTrianglePath

This project shows a simple implementation for the shortest path algorithm.

## Build project

Follow these steps to build the project:

1. Clone this project
1. Run `./gradlew build`
1. The library should be created as `build/libs/joselopes-1.0-SNAPSHOT.jar`
1. The build should end with something similar to:
```
    BUILD SUCCESSFUL in 15s
    5 actionable tasks: 5 executed
```

## Run project

It's possible to run the project in two different ways:

### User's input

With user input by command line: `java -jar build/libs/joselopes-1.0-SNAPSHOT.jar`

1. Fill in each line of the triangle path;
1. It's not possible to input negative values (only numbers and spaces);
1. Inform `EOF` to indicate that it's the end of the file;
1. The response will be shown as:
```
    Minimal path is: 1 + 1 + 1 = 3
```

### File system input

With text file reference as parameter: `java -jar build/libs/joselopes-1.0-SNAPSHOT.jar /path/to/file.txt`

The response will be shown as:
```
    Minimal path is: 1 + 1 + 1 = 3
```

## Need help?

Contact me via [jjbeto@gmail.com](mailto:jjbeto@gmail.com).
